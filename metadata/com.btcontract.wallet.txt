Categories:Money
License:GPLv3
Web Site:http://btcontract.github.io/VisualBitcoinWallet/
Source Code:https://github.com/btcontract/VisualBitcoinWallet
Issue Tracker:https://github.com/btcontract/VisualBitcoinWallet/issues

Auto Name:Bitcoin
Summary:Simple Bitcoin wallet with advanced security features
Description:
Visual Bitcoin is a standalone wallet for Android devices which does not depend
on any centralized service and gives you full control over your money.
.

Repo Type:git
Repo:https://github.com/btcontract/VisualBitcoinWallet.git

Build:1.04,30
    commit=a510a0e
    subdir=app
    submodules=yes
    gradle=yes
    srclibs=jbox2d@e1b7021e7adb2c39b047e530a4ca52d015976fb7
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=pushd $$jbox2d$$/jbox2d-library && \
        $$MVN3$$ install && \
        popd && \
        ./prepare_fdroid.sh

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.04
Current Version Code:30
