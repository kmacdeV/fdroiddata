Categories:Games
License:MIT
Author Name:GunshipPenguin
Web Site:
Source Code:https://github.com/GunshipPenguin/open_flood
Issue Tracker:https://github.com/GunshipPenguin/open_flood/issues

Auto Name:Open Flood
Summary:A simple but addictive flood fill game
Description:
Open Flood is a simple, challenging and addictive puzzle game where you must
fill the entire game board with a single color in less than the maximum number
of steps allowed.
.

Repo Type:git
Repo:https://github.com/GunshipPenguin/open_flood

Build:1.1.0,2
    commit=0f61a73
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
